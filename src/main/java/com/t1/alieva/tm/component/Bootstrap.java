package com.t1.alieva.tm.component;

import com.t1.alieva.tm.api.controller.ICommandController;
import com.t1.alieva.tm.api.controller.IProjectController;
import com.t1.alieva.tm.api.controller.IProjectTaskController;
import com.t1.alieva.tm.api.controller.ITaskController;
import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.service.*;
import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;
import com.t1.alieva.tm.controller.CommandController;
import com.t1.alieva.tm.controller.ProjectController;
import com.t1.alieva.tm.controller.ProjectTaskController;
import com.t1.alieva.tm.controller.TaskController;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.repository.CommandRepository;
import com.t1.alieva.tm.repository.ProjectRepository;
import com.t1.alieva.tm.repository.TaskRepository;
import com.t1.alieva.tm.service.*;
import com.t1.alieva.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository,taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService,projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    private void processCommand(final String command) throws AbstractFieldException, AbstractEntityNotFoundException, CommandNotSupportedException {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }

        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_LIST:
               taskController.showTasks();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_TO_PROJECT:
                projectTaskController.unbindTaskToProject();
                break;
            case TerminalConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.findTasksByProjectId();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                throw new CommandNotSupportedException();
        }
    }

    private void initLogger(){
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArgument(final String argument) throws ArgumentNotSupportedException {
        if (argument == null) {
            commandController.showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                throw new ArgumentNotSupportedException();
        }
    }

    private void exit() {
       System.exit(0);
    }

    public void run (final String[] args) throws AbstractEntityNotFoundException, AbstractFieldException, ArgumentNotSupportedException {
        if (processArguments(args)) System.exit(0);

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("OK");
            }
            catch (final Exception e)
            {
                loggerService.error(e);
                System.out.println("FAIL");
            }
        }
    }

    private void initDemoData() throws AbstractEntityNotFoundException, AbstractFieldException {
        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("BETA PROJECT", Status.COMPLETED));

        taskService.create("MEGA TASK");
        taskService.create("BETA PROJECT");
    }
    private boolean processArguments(final String[] args) throws ArgumentNotSupportedException {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
