package com.t1.alieva.tm.api.controller;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractEntityNotFoundException, AbstractFieldException;

    void unbindTaskToProject() throws AbstractEntityNotFoundException, AbstractFieldException;

}
