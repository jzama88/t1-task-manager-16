package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task add(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Integer getSize();

    List<Task> findAllByProjectId(String projectId);

}
