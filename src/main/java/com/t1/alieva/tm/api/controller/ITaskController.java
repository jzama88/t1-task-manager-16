package com.t1.alieva.tm.api.controller;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Task;

public interface ITaskController {

    void createTask() throws AbstractEntityNotFoundException, AbstractFieldException;

    void clearTask();

    void showTasks();

    void showTask(Task task);

    void showTaskById() throws AbstractFieldException;

    void showTaskByIndex() throws AbstractFieldException;

    void updateTaskByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

    void updateTaskById() throws AbstractEntityNotFoundException, AbstractFieldException;

    void removeTaskById() throws AbstractFieldException;

    void removeTaskByIndex() throws AbstractFieldException;

    void startTaskById() throws AbstractFieldException;

    void startTaskByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

    void completeTaskById() throws AbstractFieldException;

    void completeTaskByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

    void changeTaskStatusById() throws AbstractFieldException;

    void changeTaskStatusByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

    void findTasksByProjectId();

}
