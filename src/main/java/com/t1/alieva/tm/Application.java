package com.t1.alieva.tm;


import com.t1.alieva.tm.component.Bootstrap;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;

public final class Application {

     public static void main(String[] args) throws AbstractEntityNotFoundException, ArgumentNotSupportedException, AbstractFieldException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
