package com.t1.alieva.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException{

    public ArgumentNotSupportedException(){
        super("Error! Argument is incorrect...");
    }
}
