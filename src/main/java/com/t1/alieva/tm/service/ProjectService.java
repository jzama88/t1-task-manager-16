package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.service.IProjectService;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.ProjectNotFoundException;
import com.t1.alieva.tm.exception.field.*;
import com.t1.alieva.tm.model.Project;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(Comparator comparator) {
        if(comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(Sort sort) {
        if(sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public Project findOneById(final String id) throws AbstractFieldException {
        if(id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }
    @Override
    public Project findOneByIndex(final Integer index) throws AbstractFieldException {
        if(index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }
     @Override
     public Project updateById(final String id, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException{
        if(id == null || id.isEmpty()) throw new IdEmptyException();
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if(project == null) throw new ProjectNotFoundException() ;
        project.setName(name);
        project.setDescription(description);
        return project;
     }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(index == null || index < 0) throw new IndexIncorrectException();
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if(project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

     @Override
     public void remove(final Project project) throws AbstractEntityNotFoundException {

        if(project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
     }

    @Override
    public Project removeById(final String id) throws AbstractFieldException {
        if(id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeById(id);
     }

    @Override
    public Project removeByIndex(final Integer index) throws AbstractFieldException {
        if(index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project add(final Project project) throws AbstractEntityNotFoundException  {
        if(project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        return add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        if(description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return add(new Project(name, description));
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
